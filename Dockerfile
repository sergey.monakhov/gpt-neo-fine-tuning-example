FROM nvidia/cuda:10.2-cudnn8-devel

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
      curl \
      software-properties-common \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN add-apt-repository ppa:deadsnakes/ppa -y \
    && apt-get update \
    && apt-get install -y --no-install-recommends \
         python3.8 \
         python3.8-distutils \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN curl https://bootstrap.pypa.io/get-pip.py -o /tmp/get-pip.py \
    && python3.8 /tmp/get-pip.py \
    && rm -rf /tmp/get-pip.py

COPY . /usr/src/app
COPY requirements.txt /usr/src/app/

WORKDIR /usr/src/app

RUN pip install --no-cache-dir -r requirements.txt \
    && pip install --no-cache-dir deepspeed==0.3.13
