import os
from transformers import GPT2Tokenizer,GPTNeoForCausalLM

PRETRAINED_MODEL = os.getenv('PRETRAINED_MODEL')

GPT2Tokenizer.from_pretrained("{}".format(PRETRAINED_MODEL), bos_token='<|endoftext|>',
                                          eos_token='<|endoftext|>', pad_token='<|pad|>')
GPTNeoForCausalLM.from_pretrained("{}".format(PRETRAINED_MODEL))
